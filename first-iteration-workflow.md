# Briar distributed backup workflow first iteration of design

## Backup Process

![backup process](./assets/dc-briar-backup.jpg)

There is an option to backup only the account itself (meaning the cryptographic keys used to identify a Briar peer) or both the account and contacts list.  After some discussion we decided to remove this option for simplicity, and assume the peer would like to backup both.

There is also an option to send out an 'updated backup' when the contacts list has changed.  Again after some discussion it was decided to remove this option, and this would be done automatically without active involvement of the peer.


## Recovery Process

![recovery process](./assets/dc-briar-recovery.jpg)

Initially the idea was to have the recovery process involve a temporary Briar account which the secret-owner would use whilst in the process of retrieving shards from their original account.

The custodians would be added as contacts by the normal process of adding contacts in Briar (either by in-person QR code exchange or remotely via Briar links sent out-of-band). Returned shards would then be exchanged as briar messages, just as 'outgoing' shards are.

Normal Briar functionality (such as messaging) would not be available when in this 'recovery mode' to avoid complications when the original account is restored.


