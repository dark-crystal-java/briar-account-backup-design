
## Design notes for Briar distributed account backup feature design

Initial white-boarding and ideation for a feature to backup and restore a Briar account using Dark Crystal

- [1st iteration](./first-iteration-workflow.md)
- [2nd iteration](./Second-iteration-of-briar-distributed-account-backup-workflow.md)
- [3rd iteration](./third-iteration-workflow.md)
- [4th iteration](./4th-iteration-workflow.md)
- [5th iteration](./5th-iteration-workflow.md)
- [6th iteration](./6th-iteration-workflow.md)

### Design decisions for proof-of-concept

The proof-of-concept will offer a simplified version of the feature, in order to demonstrate the idea and inform subsequent iterations.

Included features:

- Automatic, incremental backup of contacts list.  When the contacts list added to, or modified, the distributed backup is updated without any involvement from the peer.

- Shard return by QR code will not involve a QR code containing the actual shard.  Instead, the QR code will be the initial pass of a handshake process which takes place by other means (for example, via bluetooth), to improve security. 

Excluded features: 

- No mandatory consent gaining from custodians.  The secret owner can of course send a message to a friend asking if they would be happy to hold a shard.  But no automated mechanism requiring their consent will be included in the proof-of-concept design.

- A health-check of current backup.  The secret-owner will be able to see confirmation that their shards have been delivered.  But no further information as to the integrity of their remote backup.  Subsequent designs will include displaying the times when the custodians were last connected, and possibly some sort of verification of the integrity of the shard.

- The recovery process requires meeting the custodians in-person to transfer shards. This mitigates security issues around confirming the identity of the secret owner.  We plan to allow this process to be done remotely, with some confirmation process to ensure shards are transferred to the right person, but to keep thing simple, this is beyond the scope of the proof-of-concept.



