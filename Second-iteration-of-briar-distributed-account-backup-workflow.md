# Second iteration of briar distributed account backup workflow 

See also:
- [Pad with questions / ideation around these issues](https://pad.briarproject.org/p/dark-crystal)

## Backup process from secret-owner's point of view

![backup1-secret-owner](https://i.imgur.com/hiL14xP.jpg)

The backup process is largely unchanged from the first iteration. The option to only backup an account, not contacts has been removed.  For simplicity we assume the user always wants to backup both their account key and their contacts.

Several open questions remain around the specifics of 'change transfer contacts' (making a new backup with different custodians) and what 'revoking' involves. After some discussion it was decided not to address these in the basic proof-of-concept, and to initially only implement the functionality to create a backup with one set of custodians.

In the screen where the existing backup is shown, a 'health-check' would also be used to display whether shards have been successfully delivered to the custodians.

Rather than assuming the peer will keep an eye on this, a warning notification could be displayed if shards are not delivered after a certain period of time.

## Backup process from custodian's point of view

![backup2-custodian](https://i.imgur.com/lXXjnlq.jpg)

The proposal is that the custodian gets a notification that they have a received a shard, but their is no mandatory consent process.

This was also discussed at length. We decided we agree that in the long-term we want an consent mechanism for custodians, but we will leave it out of the proof-of-concept as it makes the user-interface much more complicated.

Regarding consent we have three proposals:

1. No consent required - only a notification shown that a shard has been received.  Of course, the secret owner can anyway first ask the custodian if it is okay, using a normal Briar message.
2. 'weak consent'.  The shard is sent to the custodian right away, but on receiving it they are asked for their consent and if they refuse it is deleted locally, and a message sent to the secret-owner to inform them.  This has the advantage that in the 'happy path' (when everything goes well), the backup is made very quickly, and implementation on the side of the secret-owner would be fairly simple. The disadvantage is that until the custodian reads and responds to this notification, they have unwillingly accepted the shard.
3. 'strong consent'. Initially, only a message with a request for consent is transmitted to the custodian.  They only receive the shard itself after confirming. This is perhaps better from the custodians point of view, but it means a longer period when no backup is in place.

## Recovery process from secret-owner's point of view

![recovery-1-secret-owner](https://i.imgur.com/GFAuE3O.jpg)

As with the first iteration, the term 'recovery mode' is used, but here no temporary briar account is created.  We addressed question as to how things work whilst in this temporary state of being in the process of gathering shards.  Is there any password protection to enter the app at this stage?  Are the shards stored encrypted on disk?  With what key?

It was decided that at this stage no password protection would be added, and the shards could be stored unencrypted until the point where the threshold is reached.  This gives a great advantage as we don't need to worry about what happens if the password to the recovery mode is lost, or how can we secure the temporary key used for recovery mode. We decided this make sense from a security point of view, because any set of shards which does not meet the threshold gives an attacker no information about the secret.

However, to improve security, we decided that identifying information about the custodians who provided these shards would never be stored.  So an attacker could not guess the identities of the missing custodians by process of elimination.  That is, public keys or aliases of custodians are not transmitted together with returned shards.

Shards can be transferred by QR code (or perhaps, due to the limitations of how much data can be included in a QR code, the QR code contains an ephemeral key used to transmit the actual shard data by some other means).

Pictured is also an option 'add remote link' - implying another way of transferring shards remotely by some sort of out-of-band key exchange. But for now lets forget this and assume QR codes are the only way to return shards, requiring an in-person exchange and mitigating issues around confirming the identity of the secret-owner.

**'Add remote link' is not in P-o-c.**

## Recovery process from custodian's point of view

![recovery-2-custodian](https://i.imgur.com/DU1pMhM.jpg)

An option called something like 'help recover account' can be found in the options menu for the particular contact.  This option only appears if you are holding a shard for that contact.

Again there is an option 'by exchanging links' but i suggest we ignore this option for now.

